import logging
from dataclasses import dataclass
from pathlib import Path
from mutagen.mp3 import EasyMP3, HeaderNotFoundError
from model import create_tables, init_data, Artist, Album, Track
from peewee import *

@dataclass
class track_information:
    path: str
    album: str
    artist: str
    length: int
    title: str
    tracknumber: int

def build_track(audio_info, full_path):
    """
    parameter: audio_info an EasyMP3 object
    full_path: the path to the mp3 file
    """
    logging.info(f"Building data for track {full_path}")
    track_number = 0
    # Biggest note: tracknumber is sometimes not present and can be a weird
    # non-integer value. In some cases the album tag has not been present,
    # may add code in the future as a workaround for that
    if "tracknumber" in audio_info and audio_info["tracknumber"] is not None:
        track_string = audio_info["tracknumber"][0]
        try:
            track_number = int(track_string)
        except ValueError:
            track_number = int(audio_info["tracknumber"][0].split("/")[0])
    try:
        album = audio_info["album"][0]
    except KeyError:
        album = audio_info["title"][0]

    return track_information(full_path,
        album,
        # audio_info["album"][0],
        audio_info["artist"][0],
        audio_info.info.length,
        audio_info["title"][0],
        track_number        
        )

class BufferFullException(Exception):
    def __str__(self):
        return "Adding a value to a full buffer"

def add_track(track):
    logging.info(f"Adding track {track.path}")
    artist = Artist.get_or_create(name=track.artist,
                                  search_name=track.artist.lower())
    album = Album.get_or_create(name=track.album,
                                search_name=track.album.lower())
    # As noted here: https://github.com/coleifer/peewee/issues/1369
    # get_or_create returns a tuple. The first item in the tuple
    # is the object wanted.
    track = Track.get_or_create(filename=str(track.path),
                                search_name=str(track.path).lower(),
                                artist=artist[0],
                                album=album[0],
                                length=track.length,
                                title=track.title,
                                track_number=track.tracknumber)


class ProcessBuffer:
    def __init__(self):
        self._max_items = 100
        self._items = []
        self._current = 0

    @property
    def full(self):
        return len(self._items) >= self._max_items

    def add(self, item):
        if len(self._items) < self._max_items:
            self._items.append(item)
        else:
            raise BufferFullException

    def clear(self):
        self._current = 0
        self._items.clear()

    # These are what makes it iterable
    # Source: https://www.geeksforgeeks.org/iterators-in-python/
    def __iter__(self):
        return self

    def __next__(self):
        value = self._items[self._current]
        self._current = self._current + 1
        if self._current >= len(self._items):
            raise StopIteration
        if self._current >= self._max_items:
            raise StopIteration
        return value
    

def mp3_process(database, path):
    init_data(database)
    create_tables()
    buffer = ProcessBuffer()
    music_folder = Path(path)
    for i in music_folder.rglob("*/*.mp3"):
        print(i.name)
        try:
            maudio = EasyMP3(i.absolute())
            track_data = build_track(maudio, i.absolute())
            buffer.add(track_data)
        except HeaderNotFoundError:
            print(f"File {i.absolute()} has a problem")
        if buffer.full:
            for item in buffer:
                add_track(item)
            buffer.clear()
    for item in buffer:
        add_track(item)
