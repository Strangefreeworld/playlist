from dataclasses import dataclass
import random
from model import init_data, Artist, Album, Track


@dataclass
class track:
    path: str
    albumname: str
    artist: str
    length: int
    title: str

def generate_track_info(track_data):
    return track(track_data.filename,
                 track_data.album.name,
                 track_data.artist.name,
                 track_data.length,
                 track_data.title)

def generate_playlist(database, artist, output, length):
    init_data(database)
    artist_query = Artist.select().where(Artist.search_name == artist.lower())
    artist_tracks = Track.select().where(Track.artist << artist_query)
    trackdata = list(map(generate_track_info, artist_tracks))
    playlist = random.sample(trackdata, length)
    with open(output, "w") as plfile:
        plfile.write("#EXTM3U\n")
        for track in playlist:
            plfile.write(f"#EXTINF:{track.length},{track.artist} - {track.title}\n")
            plfile.write(f"{track.path}\n")
