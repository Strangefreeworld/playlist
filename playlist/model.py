from peewee import *

datastore = SqliteDatabase(None)

class BaseModel(Model):
    class Meta:
        database = datastore

class Artist(BaseModel):
    name = TextField(unique=True)
    search_name = TextField()

class Album(BaseModel):
    name = TextField(unique=True)
    search_name = TextField()

class Track(BaseModel):
    filename = TextField()
    search_name = TextField()
    artist = ForeignKeyField(Artist)
    title = TextField()
    album = ForeignKeyField(Album)
    length = IntegerField()
    track_number = IntegerField()

def create_tables():
    with datastore:
        datastore.create_tables([Artist, Album, Track])

def init_data(data_file):
	datastore.init(data_file)