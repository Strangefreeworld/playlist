import logging
import click
from generate import generate_playlist
from create import mp3_process


@click.group()
def playlist():
    pass


@playlist.command()
@click.argument("artist")
@click.argument("database", type=click.Path(exists=True))
@click.argument("output", type=click.Path(exists=False))
@click.argument("length", type=click.INT, default=10)
def generate(artist, database, output, length):
    generate_playlist(database, artist, output, length)

@playlist.command()
@click.argument("folder", type=click.Path(exists=True))
@click.argument("database", type=click.Path(exists=False))
def createdb(folder, database):
    logging.basicConfig(
        filename="mp3_process.log",
        level=logging.INFO,
        format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s",
    )
    mp3_process(database, folder)

@playlist.command()
@click.argument("folder", type=click.Path(exists=True))
@click.argument("database", type=click.Path(exists=False))
def update(folder, database):
    click.echo(f"update {folder} {database}")